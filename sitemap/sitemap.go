/* Составление карты сайта на основании данных, сохраненных в БД

 */
package sitemap

import (
	"database/sql"
	"io"
	"log"
	//	"math"

	"github.com/shopspring/decimal"
)

type SitemapFromDb struct {
	db     *sql.DB
	out    io.Writer
	logged bool
}

func New(db *sql.DB, output io.Writer) *SitemapFromDb {
	this := new(SitemapFromDb)
	this.db = db
	this.out = output
	this.logged = true
	return this
}

func (this *SitemapFromDb) SetLogged(l bool) *SitemapFromDb {
	this.logged = l
	return this
}

func (this *SitemapFromDb) Start() {
	var (
		rowLoc           string
		rowPriority      float64
		rowErr           error
		rows             *sql.Rows
		row              *sql.Row
		maxRank, minRank float64
		err              error
	)
	if this.logged {
		log.Println("Генерация карты сайта")
	}
	decimal.DivisionPrecision = 2

	// Максимальное значение.
	row = this.db.QueryRow("SELECT MAX(pagerank) FROM page")
	if err = row.Scan(&maxRank); err != nil {
		maxRank = -1
	}
	// Минимальное значение.
	row = this.db.QueryRow("SELECT MIN(pagerank) FROM page")
	if err = row.Scan(&minRank); err != nil {
		minRank = -1
	}

	io.WriteString(this.out, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\r\n")

	rows, err = this.db.Query("SELECT url, pagerank FROM page ORDER BY pagerank DESC")
	if err == nil {
		defer rows.Close()
		// карта сформирована - запишем в выходной поток
		for rows.Next() {
			if rowErr = rows.Scan(&rowLoc, &rowPriority); rowErr == nil {
				if maxRank != -1 && minRank != -1 {
					if maxRank == minRank {
						rowPriority = 1
					} else {
						rowPriority = (rowPriority - minRank) / (maxRank - minRank)
					}
				}
				if rowPriority < 0.1 {
					rowPriority = 0.1
				}

				// данные получены -> записать
				io.WriteString(this.out, "<url>")
				io.WriteString(this.out, "<loc>"+rowLoc+"</loc>")
				io.WriteString(this.out, "<priority>"+decimal.NewFromFloat(rowPriority).StringFixed(2)+"</priority>")
				io.WriteString(this.out, "</url>\r\n")
			} else {
				if this.logged {
					log.Println("Ошибка при получении строки данных: ", err)
				}
			}
		}
	} else {
		if this.logged {
			log.Println("Ошибка запроса к БД при формировании карты сайта: ", err)
		}
	}
	io.WriteString(this.out, "</urlset>")
} // end Start
