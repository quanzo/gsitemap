package main

/* генерирование карты сайта по данным, собранным gcrawler

 */

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3" // обязательно подключение драйвера, иначе ошибка
	"github.com/quanzo/gsitemap/sitemap"
)

func main() {
	var (
		fnDb      string
		fnSitemap string
		err       error
		fSitemap  *os.File
		dbCrawler *sql.DB
		ok        bool
	)

	log.SetOutput(os.Stdout) // все логи
	flag.StringVar(&fnDb, "db", "sitemap.db", "Имя файла, сгенерированного gcrawler")
	flag.StringVar(&fnSitemap, "sitemap", "sitemap.xml", "Имя файла для сохранения карты сайта")
	flag.Parse()
	if flag.Parsed() && fnDb != "" && fnSitemap != "" {
		if fSitemap, err = os.Create(fnSitemap); err == nil { // создаем файл для карты сайта
			if dbCrawler, err = sql.Open("sqlite3", fnDb); err == nil { // подключение к БД
				// файл создан, к БД подключились - создаем объект для создания sitemap
				genSitemap := sitemap.New(dbCrawler, fSitemap)
				genSitemap.SetLogged(true)
				// старт
				genSitemap.Start()
				ok = true
				log.Println("Карта сайта сформирована.")
				return
			} else {
				log.Println("Ошибка подключения к файлу БД " + fnDb + ". Error: " + err.Error())
			}
		} else {
			log.Println("Ошибка создания файла с картой сайта. Error: " + err.Error())
		}
	}
	if !ok {
		fmt.Println("Создание файла с картой сайта на основании данных, сформированных gcrawler\r\n")
		fmt.Println("\r\nПараметры:")
		flag.PrintDefaults()
	}
} // end main
